mmllib (1.4-1) unstable; urgency=medium

  [ Yogeswaran Umasankar ]
  * Team upload.
  * New upstream version 1.4.
  * Patch for AssertionError during test with Python 3.12.
    (Closes: #1058235)
  * Bumped Standards-Version to 4.6.2 + updated homepage URL in
    d/control.
  * Removed signing-key.asc no upstream signature found + added
    metadata in d/upstream.
  * Updated watch to version 4 format.
  * Updated copyright year for Files * + added 2024 for debian/*
    in d/copyright.
  * Included rules-requiring-root in d/control to fix lintian
    silent-on-rules-requiring-root.

  [ Jeroen Ploemen ]
  * Control: set Testsuite to autopkgtest-pkg-pybuild.
  * Rules: add override for dh_auto_test with a custom test command.

 -- Yogeswaran Umasankar <kd8mbd@gmail.com>  Thu, 15 Feb 2024 06:40:32 +0000

mmllib (0.3.0.post1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Re-export upstream signing key without extra signatures.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Tue, 03 May 2022 23:55:57 -0400

mmllib (0.3.0.post1-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop PyPy support
  * Drop Python 2 support
  * Bump standards version to 4.4.0 (no changes)
  * Bump debhelper compat level to 12

 -- Ondřej Nový <onovy@debian.org>  Tue, 23 Jul 2019 16:04:09 +0200

mmllib (0.3.0.post1-1) unstable; urgency=low

  * Initial release. (Closes: #853055)

 -- Dominik George <nik@naturalnet.de>  Thu, 22 Sep 2016 14:07:12 +0200
